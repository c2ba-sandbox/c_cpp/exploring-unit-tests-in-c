gen_cmake_build() {
  cmake -DCMAKE_TOOLCHAIN_FILE=./vcpkg/scripts/buildsystems/vcpkg.cmake -B build -S . -DCMAKE_INSTALL_PREFIX=./dist -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=1
}

build_release() {
  cmake --build build/ -j --config Release
}

build_release_install() {
  cmake --build build/ -j --config Release --target install
}

build_debug() {
  cmake --build build/ -j --config Debug
}

gen_clang_format() {
  clang-format --dump-config --style="{BasedOnStyle: WebKit, IndentWidth: 2, NamespaceIndentation: None, UseTab: Never}" > .clang-format
}

run_tests_release() {
  pushd build
  ctest -C Release -V
  popd
}

run_tests_debug() {
  pushd build
  ctest -C Debug -V
  popd
}

build_vcpkg_dependencies() {
  if [ ! -e ./vcpkg ]; then
    git clone https://github.com/microsoft/vcpkg
    ./vcpkg/bootstrap-vcpkg.sh -useSystemBinaries
  fi
  pushd ./vcpkg
  git checkout 2020.07
  ./vcpkg upgrade --no-dry-run
  ./vcpkg install cmocka
  popd
}

install_pacman_build_dependencies() {
  pacman -Sy --noconfirm git cmake gcc curl unzip tar make zip ninja
}

install_pacman_test_dependencies() {
  pacman -Sy --noconfirm cmake
}

full_build_release() {
  build_vcpkg_dependencies
  gen_cmake_build
  build_release_install
}

run_tests_release() {
  pushd ./build
  ctest -C Release -V
  popd
}

export_vcpkg_dependencies() {
  ./vcpkg/vcpkg export --x-all-installed --zip --output=precompiled-third-party
}