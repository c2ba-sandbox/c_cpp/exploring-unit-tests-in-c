FROM archlinux

RUN pacman -Syu --noconfirm git cmake gcc curl unzip tar make zip

WORKDIR /workdir

ADD . /workdir/

RUN git clone https://github.com/microsoft/vcpkg
RUN ./vcpkg/bootstrap-vcpkg.sh
RUN ./vcpkg/vcpkg install cmocka
# RUN cmake -DCMAKE_TOOLCHAIN_FILE=./vcpkg/scripts/buildsystems/vcpkg.cmake -B build -S . -DCMAKE_BUILD_TYPE=Release -DBUILD_TESTING=1