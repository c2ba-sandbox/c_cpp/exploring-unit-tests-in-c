cmake_minimum_required(VERSION 3.18)

project(exploring_unit_tests_in_c)

add_library(hello_world src/hello_world.c)
target_include_directories(hello_world PUBLIC include/)
file(GLOB_RECURSE INCLUDE_FILES "include/*.h")
set_target_properties(hello_world PROPERTIES PUBLIC_HEADER ${INCLUDE_FILES})

install(TARGETS hello_world
  RUNTIME DESTINATION bin
  LIBRARY DESTINATION lib
  ARCHIVE DESTINATION lib
  PUBLIC_HEADER DESTINATION include
)

if((CMAKE_PROJECT_NAME STREQUAL PROJECT_NAME) AND BUILD_TESTING)
  enable_testing()

  add_executable(test_hello_world tests/test_hello_world.c)
  target_link_libraries(test_hello_world PRIVATE hello_world)

  find_package(cmocka CONFIG REQUIRED)
  target_include_directories(test_hello_world PRIVATE ${CMOCKA_INCLUDE_DIR})
  target_link_libraries(test_hello_world PRIVATE ${CMOCKA_LIBRARIES})

  add_test(NAME test_hello_world COMMAND test_hello_world)
endif()
